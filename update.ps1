$modules = @('anaconda','chocolatey','msys2','office','visual studio','windows', 'windows store apps', 'winget')
Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName 'System.Drawing'
Add-Type -AssemblyName 'PresentationFramework'

function Assert-Admin {
    $currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
    $isAdmin = $currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)

    if (-not $isAdmin) {
        Write-Error "Error! Cannot update without administrator privileges." -ErrorAction stop -Category PermissionDenied
    }
}

function colorPicker([int]$value) {
    [int]$scalar = $value * 12 + 64
    [int]$rbinDigit = ($value + [math]::truncate($value / 8)) % 8
    Write-Host $rbinDigit
    [int]$rOffset = ($rbinDigit % 2) * $scalar
    [int]$rDec = ($scalar + (Get-Random -Minimum 0 -Maximum 20) + $rOffset) % 256
    [string]$r =  '{0:x}'-f $rDec
    Write-Host "r: ${r}"
    [int]$gbinDigit = [math]::truncate($rbinDigit / 2)
    [int]$gOffset = ($gbinDigit % 2) * $scalar
    [int]$gDec = ($scalar + (Get-Random -Minimum 0 -Maximum 20) + $gOffset) % 256
    [string]$g =  '{0:x}' -f $gDec
    Write-Host "g: ${g}"
    [int]$bbinDigit = [math]::truncate($gbinDigit / 2)
    [int]$bOffset = ($bbinDigit % 2) * $scalar
    [int]$bDec = ($scalar + (Get-Random -Minimum 0 -Maximum 20) + $bOffset) % 256
    [string]$b =  '{0:x}' -f $bDec
    Write-Host "b: ${b}"

    return "#${r}${g}${b}"
}

function Update-Modules {
    $idx = 1
    $selected.ForEach({
        $color = colorPicker $idx  
        wt -w winsys nt --tabColor $color --suppressApplicationTitle --title (Get-Culture).TextInfo.ToTitleCase("$_") Powershell -noexit -File $PSScriptRoot\functions\$_.ps1
        $idx++
    })
   
}

Write-Host  "WinSysUpdate Version 0.2.0"
Write-Warning  "Updating Windows may reboot your PC!!!"

Assert-Admin

$form = New-Object System.Windows.Forms.Form
$form.Text = "Update"
$form.Size = New-Object System.Drawing.Size(300,350)
$form.FormBorderStyle = 1
# [string]$library = $form.PSObject.Properties | Select-Object -Expand Name
# Write-Host $library 

$height = 0
$tabIdx = 0
$checkboxes = New-Object System.Collections.ArrayList
$selected = New-Object System.Collections.ArrayList

$copyright = New-Object System.Windows.Forms.Label
$copyright.Location = New-Object System.Drawing.Point(80,283)
$copyright.Size = New-Object System.Drawing.Size(200,20)
$copyright.Text = 'Open Source by Andrew Singley'
$form.Controls.Add($copyright)

$modules.ForEach({
    $checkbox = New-Object System.Windows.Forms.Checkbox 
    $null = $checkboxes.Add($checkbox)
    $checkbox.Location = New-Object System.Drawing.Size(10,(10+$height)) 
    $checkbox.Size = New-Object System.Drawing.Size(500,20)
    $checkbox.Text = (Get-Culture).TextInfo.ToTitleCase("$_")
    $checkbox.Checked = $true
    $checkbox.TabIndex = $tabIdx
    $checkbox.Tag = $_
    
    $height += 30
    $tabIdx += 1
    $null = $form.Controls.Add($checkbox)
})

$button = New-Object System.Windows.Forms.Button
$button.Location = New-Object System.Drawing.Point(100,250)
$button.Size = New-Object System.Drawing.Size(75,23)
$button.Text = 'OK'
$button.DialogResult = [System.Windows.Forms.DialogResult]::OK
$form.AcceptButton = $button
$null = $form.Controls.Add($button)

$form.Topmost = $true
$form.StartPosition = 'CenterScreen'
$result = $form.ShowDialog()

if ($result -eq [System.Windows.Forms.DialogResult]::OK)
{
    $checkboxes.ForEach({
        if ($_.checked) {
            $null = $selected.Add($_.Tag)
        }
    })
    # Update 
    Update-Modules
}