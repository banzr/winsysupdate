$Env:MSYSTEM="MINGW64"
$Env:MSYS="winsymlinks:nativestrict"
$Env:MSYS2_PATH_TYPE="inherit"

$msys2Path = cmd /c "where mingw64"
$msys2Path = $msys2Path = $msys2Path -replace "mingw64.exe", ""
$msys2Path = Join-Path -Path $msys2Path -ChildPath \usr\bin\bash.exe
$msys2Path = $msys2Path.Split()[-1]
# To start update
& $msys2Path --login -c 'yes | pacman -Syu --noconfirm'
# Rerun incase pacman needs to be restarted
& $msys2Path --login -c 'yes | pacman -Syu --noconfirm'