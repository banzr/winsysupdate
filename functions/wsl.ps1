$wslPass=Read-Host -AsSecureString What is your WSL password?
Write-Output "Password accepted. Beginning update..."
$bstr = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($wslPass)
$wslPassValue = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstr)
bash -c "echo '$wslPassValue' | sudo -S su -c 'apt update && apt upgrade'"
